﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.Domain.Services;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.HtmlHelpers;
using SportsStore.WebUI.Models;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Can_Paginate()
        {
            // Arrange
            Mock<IProductRepository> repo = new Mock<IProductRepository>();
            repo.Setup(m => m.Products).Returns(new Product[] {
                new Product {ProductID = 1, Name = "P1", Category = "sport" },
                new Product {ProductID = 2, Name = "P2", Category = "sport"},
                new Product {ProductID = 3, Name = "P3", Category = "sport"},
                new Product {ProductID = 4, Name = "P4", Category = "sport"},
                new Product {ProductID = 5, Name = "P5", Category = "watersport"},
                new Product {ProductID = 6, Name = "P6", Category = "watersport"},
                new Product {ProductID = 7, Name = "P7", Category = "watersport"},
                });

            IProductService service = new ProductService(repo.Object);
  
            // Act
            IEnumerable<Product> result = service.GetProductsByPage(null, 2);
            // Assert

            Product[] prodArray = result.ToArray();
            Assert.IsTrue(prodArray.Length == 3);
            Assert.AreEqual(prodArray[0].Name, "P5");
            Assert.AreEqual(prodArray[1].Name, "P6");
            Assert.AreEqual(prodArray[2].Name, "P7");
        }

        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            // Arrange - define an HTML helper - we need to do this
            // in order to apply the extension method
            HtmlHelper myHelper = null;
            // Arrange - create PagingInfo data
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };
            
            // Arrange - set up the delegate using a lambda expression
            Func<int, string> pageUrlDelegate = i => "Page" + i;
            
            // Act
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);
            
            // Assert
            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page1"">1</a>"
            + @"<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>"
            + @"<a class=""btn btn-default"" href=""Page3"">3</a>",
            result.ToString());
        }
    }
}
