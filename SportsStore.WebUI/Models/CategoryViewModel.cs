﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsStore.WebUI.Models
{
    public class CategoryViewModel
    {
        public string SelectedCategory { get; set; }

        public IEnumerable<Category> Categories { get; set; }
    }
}