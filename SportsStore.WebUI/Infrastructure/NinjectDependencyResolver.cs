﻿using Moq;
using Ninject;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Concrete;
using SportsStore.Domain.Entities;
using SportsStore.Domain.Services;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SportsStore.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        public IKernel Kernel { get; private set; }

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            this.Kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return this.Kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.Kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            this.Kernel.Bind<IProductRepository>().To<EFProductRepository>();
            this.Kernel.Bind<IProductService>().To<ProductService>();
            this.Kernel.Bind<ICategoryService>().To<CategoryService>();
            this.Kernel.Bind<IOrderProcessor>().To<FakeOrderProcessor>();
            this.Kernel.Bind<IAuthProvider>().To<FormsAuthProvider>();
        }
    }
}