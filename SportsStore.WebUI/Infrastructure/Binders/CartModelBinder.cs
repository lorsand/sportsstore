﻿using SportsStore.Domain.Entities;
using System.Web.Mvc;

namespace SportsStore.WebUI.Infrastructure.Binders
{
    public class CartModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            string sessionKey = "Cart";

            var session = controllerContext.HttpContext.Session;

            Cart cart = null;

            if (session != null)
            {
                cart = (Cart)session[sessionKey];

                if (cart == null)
                {
                    cart = new Cart();
                    controllerContext.HttpContext.Session[sessionKey] = cart;
                }
            }

            return cart;
        }
    }
}