﻿using SportsStore.Domain.Entities;
using SportsStore.Domain.Services;
using SportsStore.WebUI.Models;
using System.Linq;
using System.Web.Mvc;

namespace SportsStore.WebUI.Controllers
{
    public class ProductController : Controller
    {
        IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet]
        public ViewResult List(string category, int page = 1)
        {
            var products = productService.GetProductsByPage(category, page);

            var pagingInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = productService.Configuration.PageSize,
                TotalItems = products.Count()
            };

            var viewModel = new ProductsListViewModel()
            {
                Products = products,
                PagingInfo = pagingInfo,
                CurrentCategory = category
            };
            
            return View(viewModel);
        }

        public FileContentResult GetImage(int productId)
        {
            Product prod = productService.GetProducts()
                .FirstOrDefault(p => p.ProductID == productId);

            if (prod != null)
            {
                return File(prod.ImageData, prod.ImageMimeType);
            }
            
            return null;
        }
    }
}