﻿using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Controllers
{
    public class CartController: Controller
    {
        public static string Name = "Cart";

        private IProductRepository repository;

        private IOrderProcessor orderProcessor;

        public CartController(IProductRepository repository, IOrderProcessor orderProcessor)
        {
            this.repository = repository;
            this.orderProcessor = orderProcessor;
        }

        public const string IndexAction = nameof(Index);

        public ViewResult Index(Cart cart, string returnUrl)
        {
            var cartViewModel = new CartViewModel() {
                Cart = cart,
                ReturnUrl = returnUrl
            };

            return View(cartViewModel);
        }

        public const string AddToCartAction = nameof(AddToCart);

        public RedirectToRouteResult AddToCart(Cart cart, int productId, string returnUrl)
        {
            var product = this.repository
                .Products
                .Where(x => x.ProductID == productId)
                .FirstOrDefault();

            if (product != null)
            {
                cart.AddItem(product, 1);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public const string RemoveFromCartAction = nameof(RemoveFromCart);

        public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
        {
            var product = this.repository
                .Products
                .Where(x => x.ProductID == productId)
                .FirstOrDefault();

            if (product != null)
            {
                cart.RemoveLine(product);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public PartialViewResult Summary(Cart cart)
        {
            return PartialView(cart);
        }

        public const string CheckoutAction = nameof(Checkout);

        [HttpGet]
        public ViewResult Checkout()
        {
            var model = new ShippingDetails();

            return View(model);
        }

        [HttpPost]
        public ViewResult Checkout(Cart cart, ShippingDetails shippingDetails)
        {
            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }

            if (ModelState.IsValid)
            {
                orderProcessor.Process(cart, shippingDetails);
                cart.Clear();

                return View("Completed");
            }

            return View(shippingDetails);
        }
    }
}