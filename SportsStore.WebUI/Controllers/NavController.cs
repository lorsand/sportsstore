﻿using SportsStore.Domain.Services;
using SportsStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Controllers
{
    public class NavController : Controller
    {
        private readonly ICategoryService categoryService;

        public NavController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        public PartialViewResult Menu(string category = null)
        {
            var categoryViewModel = new CategoryViewModel() {
                Categories = this.categoryService.GetCategories(),
                SelectedCategory = category
            };

            return PartialView(categoryViewModel);
        }
    }
}