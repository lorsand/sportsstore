﻿using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Services
{
    public class ProductService : IProductService
    {
        private IProductRepository repository;

        public ServiceConfiguration Configuration { get; }

        public ProductService(IProductRepository repo, ServiceConfiguration config)
        {
            this.repository = repo;
            this.Configuration = config;
        }

        public ProductService(IProductRepository repo) : this(repo, new SimpleServiceConfiguration())
        {
        }

        public IEnumerable<Product> GetProducts()
        {
            return repository.Products;
        }

        public IEnumerable<Product> GetProductsByPage(string category, int page = 1)
        {
            int pageSize = Configuration.PageSize;

            var products = repository.Products
                .Where(x => category == null? true : x.Category == category)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .OrderBy(x => x.Price)
                .ToList();

            return products;
        }

        public int Count()
        {
            return repository.Products.Count();
        }
    }
}
