﻿using SportsStore.Domain.Concrete;
using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IProductService productService;

        public CategoryService(IProductService productService)
        {
            this.productService = productService;
        }

        public IEnumerable<Category> GetCategories()
        {
            var products = productService.GetProducts().ToList();

            var comparer = new CategoryNameEqualityComparer();

            IEnumerable<Category> categories = products
                .Select(x =>
                  new Category {
                      Name = x.Category,
                      ProductCount = products.Where(p => p.Category == x.Category).ToList().Count()
                  });

            var res = categories.ToList().Distinct(comparer);
 
            return res;
        }
    }
}
