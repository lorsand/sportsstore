﻿namespace SportsStore.Domain.Services
{
    public class SimpleServiceConfiguration : ServiceConfiguration
    {
        public override int PageSize
        {
            get
            {
                return 4;
            }
        }
    }
}
