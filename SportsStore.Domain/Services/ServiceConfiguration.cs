﻿namespace SportsStore.Domain.Services
{
    public abstract class ServiceConfiguration
    {
        public virtual int PageSize
        {
            get
            {
                return 100;
            }
        }
    }
}
