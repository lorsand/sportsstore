﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Services
{
    public interface IProductService
    {
        IEnumerable<Product> GetProductsByPage(string category, int page = 1);

        IEnumerable<Product> GetProducts();

        int Count();

        ServiceConfiguration Configuration { get; }
    }
}
