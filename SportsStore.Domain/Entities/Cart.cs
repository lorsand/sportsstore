﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public void AddItem(Product product, int quatity)
        {
            var foundProduct = lineCollection
                .Where(x => x.Product.ProductID == product.ProductID)
                .FirstOrDefault();

            if (foundProduct == null)
            {
                this.lineCollection.Add(new CartLine()
                {
                    Product = product,
                    Quantity = quatity
                });
            }
            else
            {
                foundProduct.Quantity += quatity;
            }
        }

        public void RemoveLine(Product product)
        {
            lineCollection.RemoveAll(l => l.Product.ProductID == product.ProductID);
        }

        public void Clear()
        {
            lineCollection.Clear();
        }

        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Product.Price * e.Quantity);
        }

        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }
    }
}
