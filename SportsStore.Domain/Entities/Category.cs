﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    public class Category
    {
        public string Name { get; set; }

        public int ProductCount { get; set; }
    }
}
